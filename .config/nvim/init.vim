" User defined settings
set number relativenumber
set shiftwidth=2
set tabstop=2
set autoindent
set smartindent
set hidden
set nobackup
set nowritebackup
set cmdheight=1
set updatetime=300
set shortmess+=c
let mapleader = " " 
set encoding=UTF-8
syntax enable
filetype plugin indent on

call plug#begin('~/.vim/plugged')
Plug 'VundleVim/Vundle.vim'

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

Plug 'preservim/nerdtree'
Plug 'preservim/nerdcommenter'

Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'ryanoasis/vim-devicons'

Plug 'tpope/vim-fugitive'

" LSP
Plug 'neovim/nvim-lspconfig'
Plug 'nvim-lua/completion-nvim'

" Rust
Plug 'rust-lang/rust.vim'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'sainnhe/gruvbox-material'
call plug#end()


" Git Fugitive
nnoremap <silent><leader>gs :Gstatus<CR>

" LSP
" General

set completeopt=menuone,noinsert,noselect
let g:completion_matching_strategy_list = ['exact', 'substring', 'fuzzy']

nnoremap <silent>gd    <cmd>lua vim.lsp.buf.declaration()<CR>
nnoremap <silent><c-]> <cmd>lua vim.lsp.buf.definition()<CR>
nnoremap <silent>K     <cmd>lua vim.lsp.buf.hover()<CR>

" Clangd
lua require'nvim_lsp'.clangd.setup{ on_attach=require'completion'.on_attach }

" Rust Analyzer
lua require'nvim_lsp'.rust_analyzer.setup{ on_attach=require'completion'.on_attach }

" Completion

inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"


" Better Cpp Syntax Highlithing

let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_class_decl_highlight = 1
let g:cpp_experimental_template_highlight = 1

" USER DEFINED FUNCTIONS AND COMMANDS
" Clang auto format
function FormatBuffer()
 if &modified && !empty(findfile('.clang-format', expand('%:p:h') . ';'))
    let cursor_pos = getpos('.')
    :%!clang-format
    call setpos('.', cursor_pos)
  endif
endfunction
autocmd BufWritePre *.h,*.hpp,*.c,*.cpp,*.vert,*.frag :call FormatBuffer()

nnoremap <leader>s :e! $MYVIMRC<CR>
nnoremap <Leader>h :%s/\<<C-r><C-w>\>//g<Left><Left>

" NerdTree Stuff

map <C-b> :NERDTreeToggle<CR>

let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'

autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" NerdCommenter Stuff

let g:NERDSpaceDelims = 1

" Fzf Stuff

let g:fzf_layout = { 'window': {'width': 0.8, 'height': 0.8}}
let $FZF_DEFAULT_OPTS='--reverse'

nnoremap <silent> <C-p> :Files<CR>


" USER DEFINED SETTINGS AFTER PLUGINS

" Gruvbox-material
if has('termguicolors')
   set termguicolors
endif

set background=dark
let g:gruvbox_material_background = 'hard'

colorscheme gruvbox-material

"Vim Airline

let g:airline_theme='base16_gruvbox_dark_hard'

